//external types
import React, { Component } from 'react';
import { connect } from 'react-redux';
import socketIOClient from 'socket.io-client';

//internal types
import * as actions from './common/actions/index';
import * as selectors from './common/selectors/index';

/**
 * Index  PageComponent.
 * @param {Object} items array of items
 * @returns {React$Element<any>} HTML markup for the component.
 */

class Index extends Component {
  /**
   * Sets initial component state
   * @param {Object} props Props
   * @returns {void}.
   */
  constructor() {
    super();
    this.state = {
      response: false,
      endpoint: {}
    };
  }
  /**
   * Component did update
   * @returns {void}
   */
  componentDidMount() {

    /**
     * Set up data provider with API and run socketIOClient
     */
    // const { endpoint } = this.state;
    // const socket = socketIOClient(endpoint);
    // socket.on("FromAPI", data => fetchSuccess(data));
  }
  fetchSagaItems = () => {
    const { fetchSagaItems } = this.props;
    fetchSagaItems();
  };
  render() {
    const { items } = this.props;
    return (
      <React.Fragment>
        <button onClick={() => this.fetchSagaItems()}>Button</button>
        {items.map(item => <p key={item.name}>{item.name}</p>)}
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = {
  fetchSagaItems: actions.fetchSagaItems,
};

/**
 * Map State to props
 * @param {StateType} state State
 * @returns {Object} Props object
 */
const mapStateToProps = state => ({
  items: selectors.selectData(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
