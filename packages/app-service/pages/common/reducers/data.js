import * as constants from '../constants/index';

/**
 * Default data state
 * @type {{items: Array}}
 */
const defaultState = [];

/**
 * Data reducer
 * @param {Object} state State
 * @param {Object} action Action
 * @return Array
 */
const data = (state = defaultState, action = null) => {
  const actions = {
    [constants.ADD_ITEM_SUCCESS]: () => {
      return [...state, ...action.data];
    },
  };
  return (action && actions[action.type]) ? actions[action.type]() : state;
};

export default data;
