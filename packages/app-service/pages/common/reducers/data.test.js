import data from './data';

describe('Reducer data', () => {
  test('has default state', () => {
    expect(data()).not.toBeUndefined();
  });
  test('passes the state on unknown action', () => {
    const someState = '123';

    expect(data(someState)).toEqual(someState);
  });
});
