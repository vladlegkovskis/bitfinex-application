import { call, put, takeLatest } from 'redux-saga/effects';
import * as constants from '../constants/index';
import * as actions from '../actions/index';

const api = url => fetch(url).then(response => response.json());

/**
 * Fetch saga
 */
export function* fetchPerson() {
  console.info(`Will fetch data for`);
  try {
    const person = yield call(api, YOUR_API);
    if (person) {
      console.info(`Fetch success`);
      const { results } = person;
      yield put(actions.fetchSuccess(results));
    }
  } catch (e) {
    console.error(`Fetch success`);
    console.log(e);
  }
}

/**
 * Root saga
 */
function* rootSaga() {
  yield takeLatest(constants.ADD_ITEM_REQUEST, fetchPerson);
}

export default rootSaga;
