import * as selectors from './';

describe('Selectors', () => {
    test('selectData', () => {
        const state = {
          data: {
              items: [],
          }
        };
        expect(selectors.selectData(state)).toEqual(state.data);
    });
});
