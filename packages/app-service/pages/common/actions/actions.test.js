import * as actions from './index';
import * as constants from '../constants/index';

describe('Actions', () => {
  describe('fetchSagaItems', () => {
    const action = actions.fetchSagaItems();
    test('creates an action to fetch items', () => {
      expect(action).toHaveProperty('type', constants.ADD_ITEM_REQUEST);
    });
  });
  describe('fetchSuccess', () => {
    test('dispatch matches page data', () => {
      const data = [];
      const expectedAction = {
        type: constants.ADD_ITEM_SUCCESS,
        data,
      };
      expect(actions.fetchSuccess(data)).toEqual(expectedAction);
    });
  });
});
