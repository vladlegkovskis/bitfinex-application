import * as constants from '../constants/index';

export const fetchSagaItems = () => ({ type: constants.ADD_ITEM_REQUEST });
export const fetchSuccess = data => ({ type: constants.ADD_ITEM_SUCCESS, data });
