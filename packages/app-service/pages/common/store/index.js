import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers/index';
import rootSaga from '../sagas/index';

/**
 * Configure store using initial state
 * @param {Object} initialState Initial state
 * @return {Store} Configured store
 */
function configureStore(initialState) {
  const windowExist = typeof window === 'object';
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers = (windowExist && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
    || compose;

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );
  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
  };

  store.runSagaTask();
  return store;
}

const store = configureStore();

export { configureStore, store };
