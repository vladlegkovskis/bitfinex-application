# WIP: Bitfinex app

## Prerequisites

- node8
- yarn

## Install

1. Clone this repository.
2. Run `yarn`.

## Running the application

1. Run `yarn start` to start the application
2. Check it out under:  `http://localhost:3000`

## Solution

### Project structure

Project is divided into two packages using yarn workspaces. All packages are under `packages` folder.


#### `@bitfinex/app-service`

Render for both on server and client. SSR by `Next.js`.


#### `@bitfinex/data-service`

Tool to fetch data from BitFinex API


### Approach

The project status is WIP and approach might change

### Libs/tools used

* [ES8](https://www.ecma-international.org/ecma-262/8.0/)
* [NextJS](https://github.com/zeit/next.js)
* [React](https://facebook.github.io/react/)
* [semantic-ui-react](https://react.semantic-ui.com)
* [Redux](http://redux.js.org/)
* [Redux Saga](https://redux-saga.github.io/redux-saga/)
* [axios](https://github.com/axios/axios)
* [Jest](http://facebook.github.io/jest/)
* [Express](https://expressjs.com/)
* [Babel](https://babeljs.io/)
* [Webpack](https://webpack.js.org/)
* [ESLint](http://eslint.org/)
* [socket.io](https://socket.io/)

## License